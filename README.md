# SaTSviz
SaTSviz aims to provide QGIS an additional feature of analysis by utilising SaTScan. All input files needed for SaTScan are created by the plugin and the output files form it are read by the plugin and fed in to QGIS to visualise the analysis results. This plugin provides benefit to both the softwares.
